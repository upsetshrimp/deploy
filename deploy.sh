#! /bin/bash
# Author: upsetshrimp
# This is a script to deploy my Arch/manjaro setup
# with all the essentials
# I tried to decouple as much of the code as possible
# that's why there are functions like "is_installed" or "try-cmd"
# this is a work in progress
# TODO actually call the functions and test them out
# TODO make globbing and literals work in commands
# TODO integrate conf.py into this to restore configs as you install things
# TODO mess around with a python wrapper, might be easier


logfile="$HOME/arch_deployer.log"
pacmanops="-S --noconfirm"
yayops="-S --nodiffmenu"
declare -a essentials=(
    git
    openssh
    emacs
    openvpn
)
declare -a aur=(
    polybar
    pandoc-bin
    dmscripts-bin
)
try-cmd(){
    cmd=$1
    if $cmd | tee -a "$logfile"; then
        return 0
    fi
    return 1
}
install_essentials(){
    echo "Installing esential apps"
    for pkg in "${essentials[@]}"; do
        if is_installed "$pkg"; then
            echo "$pkg is already installed, skipping..."
        else
            sudo pacman -S --noconfirm "$pkg" && echo "installed $pkg"
        fi
     done
}
install_yay(){
    echo "Installing yay AUR helper"
    echo "This proccess requires root access"
    cd /opt
    sudo git clone https://aur.archlinux.org/yay-git.git
    sudo chown -R "${USER}:${USER}" ./yay-git
    cd yay-git
    makepkg -si
    if [[ $? -eq 0 ]]; then
        echo "Yay installed succesfully"
        echo "Updating AUR repos..."
        sudo yay -Syu
    fi
}
install_aur_packages(){
    echo "Installing AUR packages using yay"
    for pkg in "${aur[@]}"; do
        if is_installed "$pkg"; then
            echo -e "$pkg is already installed, skipping..."
        else
            yay "$yayops" "$pkg" && echo "installed $pkg"
        fi
    done
}
install_oh_my_zsh(){
    echo "Installing Oh my zsh"
    sh -c "$(curl -fsSL htps://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
}
install_spacemacs(){
    if ! is_installed emacs; then
        echo "Emacs not installed, installing..."
        sudo pacman "${pacmanops}" emacs
    fi
    gitcmd = "git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d"

    if try-cmd "$gitcmd"; then
        echo "spacemacs downloaded, run it to install plugin layers"
    fi

}
install_dmenu_sripts_dt(){
    if is_installed "pandoc-bin"; then
        yay -S dmscripts-git && return 0
    else
        echo "pandoc-bin not installed, try installing manually then call this function again"
        return 1
    fi
}
is_installed(){
    if pacman -Qs $1 > /dev/null; then
        return 0
    fi
    return 1
}
apply_configs(){
    # Will need to be run after everything else, and then override with my files
    # Something something "config --clone-and-checkout"
    # TODO for config (why is this todo here again?) figure out
    # branching so that laptop and workstation seamlessly merge
    # and rebase
    echo "TODO"
}
bootstrap_yadm(){
    # call the 
    echo "TODO"
}
